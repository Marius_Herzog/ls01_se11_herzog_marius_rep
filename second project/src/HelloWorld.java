
public class HelloWorld {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	   //App:Password: YZ55eMYJtP6UuTP7zjeF
	   
	   System.out.print("Marius Herzog\n\n");
	   System.out.println("Aufgabe 1:\n");
	   System.out.print("Hallo du sch�ne \"Welt\".\n");
	   System.out.print("Du bist so sch�n\n\n");
	   // print hat keinen Zeilenumbruch und println macht einen automatisch
	   
	   
	   System.out.println("Aufgabe 2:\n");
	   System.out.println("   +");
	   System.out.println("  +++");
	   System.out.println(" + + +");
	   System.out.println("   +");
	   System.out.println("   +\n");
	   
	   
	   System.out.println("Aufgabe 3:\n");
	   System.out.printf( "|%.2f|\n" , 22.4234234);
	   System.out.printf( "|%.2f|\n" , 111.2222);
	   System.out.printf( "|%.2f|\n" , 4.0);
	   System.out.printf( "|%.2f|\n" , 1000000.551);
	   System.out.printf( "|%.2f|\n\n" , 97.34);
	   
	   
	   System.out.println("Konsolenausgabe �bung 2:\n");
	   System.out.println("Aufgabe 1:\n");
	   String s = "*";
	   System.out.printf("%4s%s%n", s,s);
	   System.out.printf("%s%7s%n", s,s);
	   System.out.printf("%s%7s%n", s,s);
	   System.out.printf("%4s%s%n\n", s,s);
	   
	   
	   System.out.println("Aufgabe 2:\n ");
	   String s3 = "=";
	   String s4 = "*";
	   System.out.printf( "0!%6s%20s%4d\n", s3, s3, 1 );
	   System.out.printf( "1!%6s%2d%18s%4d\n", s3, 1, s3, 1 );
	   System.out.printf( "2!%6s%2d%2s%2d%14s%4d\n", s3, 1, s4, 2, s3, 2 );
	   System.out.printf( "3!%6s%2d%2s%2d%2s%2d%10s%4d\n", s3, 1, s4, 2, s4, 3, s3, 6 );
	   System.out.printf( "4!%6s%2d%2s%2d%2s%2d%2s%2d%6s%4d\n", s3, 1, s4, 2, s4, 3, s4, 4, s3, 24 );
	   System.out.printf( "5!%6s%2d%2s%2d%2s%2d%2s%2d%2s%2d%2s%4d\n", s3, 1, s4, 2, s4, 3, s4, 4, s4, 5, s3, 120 );
	   System.out.printf( "\n" );
	   
	   
	   System.out.println("Aufgabe 3:\n ");
	   String s1 = "Celsius";
	   String s2 = "|";
	   System.out.printf( "Fahrenheit%2s%9s\n", s2, s1);
	   System.out.printf( "----------------------\n" );
	   System.out.printf( "-20%9s", s2 );
	   System.out.printf( "%9.2f\n" , -28.8889);
	   System.out.printf( "-10%9s", s2 );
	   System.out.printf( "%9.2f\n" , -23.3333);
	   System.out.printf( "+0%10s", s2 );
	   System.out.printf( "%9.2f\n" , -17.7778);
	   System.out.printf( "+20%9s", s2 );
	   System.out.printf( "%9.2f\n" , -6.6667);
	   System.out.printf( "+30%9s", s2 );
	   System.out.printf( "%9.2f\n" , -1.1111);
	  
	}

}
