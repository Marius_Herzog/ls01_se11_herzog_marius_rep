﻿import java.util.Scanner;

class Fahrkartenautomat {
	
	public static float fahrkartenbestellungErfassen (Scanner tastatur) {
	
		byte anzahlTickets;
		float ticketpreis;
		
		System.out.print("Ticketpreis: ");
	    ticketpreis = tastatur.nextFloat();
	    System.out.print("Wie viele Tickets? ");
	    anzahlTickets = tastatur.nextByte();
	    
	  return ticketpreis * anzahlTickets;
		
	}
    
	public static float fahrkartenBezahlen (float zuZahlenderBetrag, Scanner tastatur) {
		
		float eingezahlterGesamtbetrag;
		float eingeworfeneMünze;
		eingezahlterGesamtbetrag = 0.00f;
	    while(eingezahlterGesamtbetrag < zuZahlenderBetrag){
	    	   System.out.printf("Noch zu zahlen: %.2f\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextFloat();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	           
    } 
	       
    return eingezahlterGesamtbetrag - zuZahlenderBetrag;
		
	}
	
	public static void fahrkartenAusgeben() {
		 System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");

	}
	
	public static void rückgeldAusgeben(float rückgabebetrag) {
		
    if(rückgabebetrag > 0.0)
    {
 	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f\n", (rückgabebetrag));
 	   System.out.println("wird in folgenden Münzen ausgezahlt:");

        while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
        }
        while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
        }
        while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
        }
        while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("20 CENT");
	          rückgabebetrag -= 0.2;
        }
        while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
        }
        while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("5 CENT");
	          rückgabebetrag -= 0.05;
        }
    }

    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir wünschen Ihnen eine gute Fahrt.");
	
	}
	
	
		public static void main(String[] args) {
       Scanner tastatur = new Scanner(System.in);
      
       float zuZahlenderBetrag;
       float rückgabebetrag;
       float eingezahlterGesamtbetrag;
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);
       fahrkartenAusgeben();
       rückgeldAusgeben(rückgabebetrag);
      
       
   }
}

