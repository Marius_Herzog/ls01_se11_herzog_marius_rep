import java.util.Scanner;

public class Addition {
//Zugriffsmodifizierer    	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		double zahl1, zahl2, erg;        //Lokale Variable
              
		hinweis();        //Methodenaufruf
		
		zahl1 = eingabe();       
		zahl2 = eingabe();       

        erg = addition(zahl1, zahl2);        
                   //Argument(zB.: zahl1)
        ausgabe(zahl1, zahl2, erg);          
        
        
	}
	                   
	    public static void hinweis() {     
    	
	    	//1.Programmhinweis
        System.out.println("Hinweis: ");
        System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");         
	                                                                               
        return;        
    }
	                   
	    public static void ausgabe(double zahl1, double zahl2, double erg) {      

	    	
	    	//2.Ausgabe                                                     
        System.out.println("Ergebnis der Addition");                               
        System.out.printf("%.2f + %.2f = %.2f", zahl1, zahl2, erg);                
        
	    
	}
	                     //Methodenname         //Lokale Variable
        public static double addition(double zahl1, double zahl2) {       //Methodenkopf    
        	           //R�ckgabedatentyp       //Parameter
        double erg;       
        	
        //3.Verarbeitung                                                 ////Methodendefinition ist Kopf und Rumpf
        erg = zahl1 + zahl2;                                             //Methodenrumpf zwischen {}
        
        return erg;       //R�ckgabewert
        
	}
                         
        public static double eingabe() {       
     
        double zahl;	  
        	
        Scanner sc = new Scanner(System.in);                      
        //4.Eingabe                                               
        System.out.println("Eine Zahl eingeben:");
        zahl = sc.nextDouble();
        
        return zahl;
        
        
        }
            
}
